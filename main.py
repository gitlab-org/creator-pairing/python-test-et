import sentry_sdk

sentry_sdk.init(
    "http://glet_a32c54fa567522a8c8a6dc739d42ee9f@127.0.0.1:3000/api/v4/error_tracking/collector/1",

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0,
)

division_by_zero = 1 / 0
